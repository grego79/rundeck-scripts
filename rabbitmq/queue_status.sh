#!/bin/bash

getQueueList(){
    curl -s -u $RD_OPTION_USER:$RD_OPTION_PASSWORD http://$RD_OPTION_HOST:$RD_OPTION_PORT/api/queues/%2F/ | jq . | grep name | cut -c 13- | sed 's/..$//' | sed 's/^.//'  > /tmp/calificaciones_queues.list
    cat /tmp/calificaciones_queues.list | jq -R -s 'split("\n")' | jq '.[:-1]' > /tmp/calificaciones_queues.json
}

getQueueMessages(){
for QUEUE in $(cat /tmp/calificaciones_queues.list)
do
    printf $QUEUE" "; curl -s -u $RD_OPTION_USER:$RD_OPTION_PASSWORD http://$RD_OPTION_HOST:$RD_OPTION_PORT/api/queues/%2F/$QUEUE | jq .messages
done > /tmp/calificaciones_queues_status.list
}

sortQueues(){
    sort -k2nr /tmp/calificaciones_queues_status.list
}

main(){
    getQueueList
    getQueueMessages
    sortQueues
}

main
