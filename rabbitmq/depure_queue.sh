#!/bin/bash

getQueueInfo(){
    echo "Mensajes actuales en la cola :"
    printf $RD_OPTION_QUEUE" "; curl -s -u $RD_OPTION_USER:$RD_OPTION_PASSWORD http://$RD_OPTION_HOST:$RD_OPTION_PORT/api/queues/%2F/$RD_OPTION_QUEUE | jq .messages
}

purgeQueue(){
    echo "Depurando..."
    curl -s -u $RD_OPTION_USER:$RD_OPTION_PASSWORD -XDELETE http://$RD_OPTION_HOST:$RD_OPTION_PORT/api/queues/%2F/$RD_OPTION_QUEUE/contents
}

main(){
    getQueueInfo
    purgeQueue
    getQueueInfo
}

main
