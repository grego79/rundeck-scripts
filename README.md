# Rundeck scripts
 
 Some curl based bash scripts that I've been using to automatize tasks around CI/CD tools. Mostly used in Rundeck instances, so you'll see many variables related to it.

## Artifactory

- **add_user_to_group**: Used to add users to Artifactory groups to get permissions using the artifactory API. It first gather the group list, parse the list to a json file. This json is used by Rundeck to create a dropdown list to select the groups to add the users to. Then hits the API for each group and user using curl.

- **get_repo_size**: The Artifcatory API does not offer a method to get the total used space per repo (only in the UI). This scripts gather all the repos and artifacts parsing the initial response from the API, then sums all the size of the artifacts in each one and outputs a list sorted by used space in MiB. Useful to advice developers to clean unused dockers images.

- **get_docker_tags**: Lists docker images, showing tag and creation timestamp from a specified repository. Was having trouble using the Artifactory's docker API to get this done, so I ended up parsing it with this script using AQL.

## RabbitMQ

- **queue_status.sh**: Parses the json response to get only the queue names from rabbitmq API. Then use this list to get the messages field from each queue and outputs the name of the queue and the number of messages.

- **depure_queue.sh**: Uses the list created from the previous scripts to get a dropdown list to select the queue needed to purge. Sents a DELETE request to the rabbitmq API.
