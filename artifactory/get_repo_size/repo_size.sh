#!/bin/bash

getConfig(){
    HOST=$RD_OPTION_ARTIFACTORY_HOST
    USER=$RD_OPTION_ARTIFACTORY_USER
    PASSWD=$RD_OPTION_ARTIFACTORY_PASSWD
}

getRepoList(){
    echo "Obteniendo lista de repositorios..."
    curl -s -X POST -u $USER:$PASSWD -H "Content-Type: text/plain" https://$HOST/artifactory/api/search/aql -d 'items.find({"type":"folder"})' | grep repo | grep -v path | grep -v name | uniq > /tmp/json_response
    cat /tmp/json_response | cut -c 13- | sed 's/..$//' > /tmp/repo_list
}

getRepoSize(){

echo "Calculando tamaño..."
for REPO in $(cat /tmp/repo_list) 
do
    printf $REPO; printf " "
    curl -s -X POST -u $USER:$PASSWD -H "Content-Type: text/plain" https://$HOST/artifactory/api/search/aql -d 'items.find({"repo":"'$REPO'"})' | grep size | awk '{print $3}' | sed 's/.$//' | awk '{ SUM += $1} END { print SUM / 1024 / 1024, "MiB" }' 
done > /tmp/repo_size
}

sortRepos(){
    echo "Ordenando por tamaño..."
    sort -k2 -n /tmp/repo_size
}

main(){
    getConfig
    getRepoList
    getRepoSize
    sortRepos
}

main