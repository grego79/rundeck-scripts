#!/bin/bash

getConfig(){
    ART_HOST=$RD_OPTION_ARTIFACTORY_HOST
    ART_USER=$RD_OPTION_ARTIFACTORY_USER
    ART_PASSWD=$RD_OPTION_ARTIFACTORY_PASSWD
    ART_REPO=$RD_OPTION_REPOSITORIO
    ART_PHASE=$RD_OPTION_PHASE
}

checkRepoExists(){
    curl -s -X POST -u $ART_USER:$ART_PASSWD -H "Content-Type: text/plain" https://$ART_HOST/artifactory/api/search/aql -d 'items.find({"type":"folder"})' | grep repo | grep -v path | grep -v name | uniq | cut -c 13- | sed 's/..$//' | grep -v local | grep $ART_REPO > /dev/null
    if [ $? -eq 1 ]; then
        echo "El repositorio especificado no existe!!"
        exit 1
    fi 
}

listDockerTags(){
    echo -e "PHASE"' \t '" NAME"' \t '"TAG"' \t '"CREATED"
    curl -s -X POST -u $ART_USER:$ART_PASSWD -H "Content-Type: text/plain" https://$ART_HOST/artifactory/api/search/aql -d 'items.find({"repo":"'$ART_REPO'"})' | jq '.results[] | "\(.path) \(.created)"' | grep $ART_PHASE | tr -d '"' | tr / " " | sort -u -k3,3 | rev | cut -c23- | rev | sort -k4
}

main(){
    getConfig
    checkRepoExists
    listDockerTags
}

main