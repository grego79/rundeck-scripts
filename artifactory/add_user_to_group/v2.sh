#!/bin/bash

checkVars(){
    if [ -z $RD_OPTION_GROUPS ]; then
        echo "No se indicaron uno o mas grupos!"
        exit 1
    fi
    
    if [ -z $RD_OPTION_USERS ]; then
        echo "No se indicaron uno o mas usuarios!"
        exit 1
    fi
}

updateGroupList(){
    curl -s -u $RD_OPTION_ARTIFACTORY_USER:$RD_OPTION_ARTIFACTORY_PASSWD https://$RD_OPTION_ARTIFACTORY_HOST/artifactory/api/security/groups | grep name | cut -c 13- | sed 's/..$//' | jq -R -s 'split("\n")' | jq '.[:-1]' > /tmp/artifactory_groups.json
    echo "Se actualizó la lista de grupos"
}

addUsers(){
for GROUP in $RD_OPTION_GROUPS
do
    for USERS in $RD_OPTION_USERS
    do
        echo "Se agrega el usuario" $USERS "en el grupo "$GROUP
        curl -s -L -X POST -u $RD_OPTION_ARTIFACTORY_USER:$RD_OPTION_ARTIFACTORY_PASSWD -H "Content-type:application/json" https://$RD_OPTION_ARTIFACTORY_HOST/artifactory/api/security/groups/$GROUP -d '{"name":"'$GROUP'","realm":"internal","userNames":["'$USERS'"]}'
    done    
done
}


main(){
    if [ $RD_OPTION_UPDATE_GROUPS -eq "Si" ]; then
        updateGroupList
        exit 0
    else
        updateGroupList
        checkVars
        addUsers
    fi
}

main