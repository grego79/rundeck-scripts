#!/bin/bash

updateGroupList(){
    curl -s -u $RD_OPTION_ARTIFACTORY_USER:$RD_OPTION_ARTIFACTORY_PASSWD https://$RD_OPTION_ARTIFACTORY_HOST/artifactory/api/security/groups | grep name | cut -c 13- | sed 's/..$//' | jq -R -s 'split("\n")' | jq '.[:-1]' > /tmp/artifactory_groups.json
}

addUsers(){
for GROUP in $RD_OPTION_GROUPS
do
    for USERS in $RD_OPTION_USERS
    do
        echo "Se agrega el usuario" $USERS "en el grupo "$GROUP
        curl -s -L -X POST -u $RD_OPTION_ARTIFACTORY_USER:$RD_OPTION_ARTIFACTORY_PASSWD -H "Content-type:application/json" https://$RD_OPTION_ARTIFACTORY_HOST/artifactory/api/security/groups/$GROUP -d '{"name":"'$GROUP'","realm":"internal","userNames":["'$USERS'"]}'
    done    
done
}

main(){
    updateGroupList
    addUsers
}

main